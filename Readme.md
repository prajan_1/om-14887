# OM-14887 Reproduce Steps

* Start the mysql container by running `docker-compose up -d mysql`

*  Find the container id of the mysql container
```
docker ps | grep mysql
```

* AFter getting the container, import the parcel schema from 2.0.15.x release. Replace <containerid> with your container id from step [2]
```
cat backup.sql | docker exec -i <containerid> /usr/bin/mysql -u root --password=admin123 parcel
```

* Now start the parcel component in upgrade mode.
```
docker-compose up -d parcel
```

* Monitor the logs to see the exception.
```
docker-compose logs -f parcel
```